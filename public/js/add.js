// Add Data Product in dashboard admin
async function addDataCar() {
  const data = {
    name: document.getElementById("addNameCar").value,
    price: document.getElementById("addPriceCar").value,
    size: document.getElementById("addSizeCar").value,
    // imageUrl: document.getElementById("img").value,

  };
  if ( data.name == "" ) {
    alert("Data Tidak Boleh Kosong!");
  }
  else if ( data.price == "") {
    alert("Data Tidak Boleh Kosong!");
  }
  else if ( data.size == "") {
    alert("Data Tidak Boleh Kosong!");
  }
  else if ( data.user_id == "") {
    alert("Data Tidak Boleh Kosong!");
  }
   else {
    const restAPI = await axios.post("/api/cars", data);
    if (restAPI.data.status) {
      alert("Berhasil Tambah Data");
    } else {
      alert("500 Server Error [Tidak Dapat Menambahkan data]");
    }
  }
}
