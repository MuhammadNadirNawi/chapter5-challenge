const router = require("express").Router();
const carsControllers = require('../controllers/carsControllers')
const adminCarsControllers = require('../controllers/admin/carsControllers')

const uploader = require('../middleware/uploader')

router.get('/api/cars', carsControllers.getCars)
router.post('/api/cars', uploader.single('image'), carsControllers.createCar)
router.get('/api/cars/:id', carsControllers.getCarById)
router.put('/api/cars/:id', uploader.single('image'), carsControllers.updateCar)

router.get('/', adminCarsControllers.getCars)
router.get('/addCar', adminCarsControllers.addCarspage)
router.post('/addCar', uploader.single('image'), adminCarsControllers.createCar)
router.get('/updateCar/:id', adminCarsControllers.updateCarPage)
router.post('/updateCar/:id',uploader.single('image'), adminCarsControllers.updateCar)
router.get('/delete/:id', adminCarsControllers.deleteCar)
router.get('/smallCar', adminCarsControllers.smallCar)
router.get('/mediumCar', adminCarsControllers.mediumCar)
router.get('/largeCar', adminCarsControllers.largeCar)



module.exports = router
