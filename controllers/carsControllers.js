const imagekit = require("../lib/imagekit");
const { cars } = require("../models");

async function getCars(req, res) {
  try {
      const responseData = await cars.findAll()
      res.status(200).json({
          'msg': 'success',
          'data': responseData
      })
  } catch (err) {
      console.log(err.message)
  }
}

async function createCar(req, res) {
  try {
      // process file naming        
      const split = req.file.originalname.split('.')
      console.log(split)
      const extension = split[split.length - 1]
      console.log(extension)

      const imageName = req.file.originalname + '.' + extension
      console.log(imageName)

      // upload file 
      const img = await imagekit.upload({
          file: req.file.buffer,
          fileName: imageName
      })

      console.log(img)

      const name = req.body.name
      const size = req.body.size
      const price = req.body.price
      // const { name, size } = req.body

      const newCar = await cars.create({ 
          name: name,
          price: price,
          size: size,
          imageUrl: img.url,
      })
      
      res.status(200).json({
          'status': 'success',
          'data': newCar
      })
  } catch (err) {
      res.status(400).json({
          'message': err.message
      })
  }
}

// get atau retrieve function controller
async function getCarById(req, res) {
  try {
      const id = req.params.id
      const responseData = await cars.findByPk(id)
      // console.log(product)
      if (responseData === null) {
          res.status(404).json({
              'message': `data pada ${id} tersebut tidak ada`
          })
      }

      res.status(200).json({
          'data': responseData
      })
  } catch (err) {
      console.log(err.message)
  }
}


// update data
async function updateCar(req, res) {
  try {
    // process file naming        
    const split = req.file.originalname.split('.')
    console.log(split)
    const extension = split[split.length - 1]
    console.log(extension)

    const imageName = req.file.originalname + '.' + extension
    console.log(imageName)

    // upload file 
    const img = await imagekit.upload({
        file: req.file.buffer,
        fileName: imageName
    })

    console.log(img)

      const name = req.body.name
      const size = req.body.size
      const price = req.body.price
      const id = req.params.id
      const responseData = await cars.update({
        name: name,
        price: price,
        size: size,
        imageUrl: img.url,
      }, {
          where: {
              id
          }
      })

      res.status(200).json({
          'success': true,
          'data': responseData
      })
  } catch (err) {
      console.log(err.message)
  }
}

module.exports = {
   getCars,
   createCar,
   getCarById,
   updateCar
}
