const { cars } = require("../../models");
const imagekit = require("../../lib/imagekit");



async function getCars(req, res) {
  const items = await cars.findAll()
  res.render("index", {
      items,
  });
};

async function addCarspage(req, res) {
  res.render("create");
};


async function createCar(req, res) {
  const imageName = req.file.originalname
 
  const img = await imagekit.upload({
      file: req.file.buffer,
      fileName: imageName,
  })
  const imageUrl = img.url
  const { name, price } = req.body
  const size = req.body.size?.toLowerCase()
  await cars.create({ 
      name: name,
      price: price,
      imageUrl: imageUrl,
      size: size,
  })
  res.redirect("/")
};

async function updateCarPage(req, res) {
  const id = req.params.id;
  const items = await cars.findByPk(id)
  res.render("update", {
      items,
  });
}


async function updateCar(req, res) {
  const id = req.params.id;
  const imageName = req.file.originalname
  // upload file 
  const img = await imagekit.upload({
      file: req.file.buffer,
      fileName: imageName,
  })
  const imageUrl = img.url
  const { name, price } = req.body
  const size = req.body.size?.toLowerCase()
  await cars.update({   
      name: name,
      price: price,
      imageUrl: imageUrl,
      size: size,
  },
  {
      where:{id},
  })
  res.redirect("/")
};


async function deleteCar(req, res) {
  const id = req.params.id;
  await cars.destroy({ where: { id } });
  res.redirect("/");
}

async function smallCar(req, res) {
  const items = await cars.findAll({where: {size: "small"}})
  res.render("index", {
    items,
});
}

async function mediumCar(req, res) {
  const items = await cars.findAll({where: {size: "medium"}})
  res.render("index", {
    items,
});
}

async function largeCar(req, res) {
  const items = await cars.findAll({where: {size: "large"}})
  res.render("index", {
    items,
});
}




module.exports = {
  getCars,
  addCarspage,
  createCar,
  updateCarPage,
  updateCar,
  deleteCar,
  smallCar,
  mediumCar,
  largeCar
}
